import { LitElement, html, css } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { provide, createContext } from "@lit/context";

export const uiContext = createContext(Symbol("ui-context"));

/**
 * An example element.
 *
 * @fires count-change - Indicates when the count changes
 * @slot - This element has a slot
 * @csspart button - The button
 */

@customElement('kub-context')
export class KubContext extends LitElement {

  @provide({ context: uiContext })
  @property({ attribute: false })
  uiDataObject: DataObjectInterface = {
    webpage_language: "en"
  };

  constructor() {
    super();

    // get the webpage_language metadata from localStorage
    let webpage_language = localStorage.getItem("kub-context:webpage_language");
    if (webpage_language !== null) {
      this.uiDataObject.webpage_language = webpage_language;
    }

    // get the webpage_language metadata from html/@lang
    webpage_language = document.documentElement.getAttribute("lang");
    if (webpage_language !== null) {
      this.uiDataObject.webpage_language = webpage_language;
    }    

    console.log(webpage_language);

  }

  static override styles = [
    css`
      .container {
        display: inline;
      }
    `
  ];

  override render() {
    return html`
      <div class="container">
        <output>${JSON.stringify(this.uiDataObject)}</output>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'kub-context': KubContext;
  }
}

export interface DataObjectInterface {
  webpage_language: string;
}
