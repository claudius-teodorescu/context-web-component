# Context Web Component


## Scope

This web component is designated to provide contexts for other descendent web components, within an web application.


## Structure

It is based upon [LitElement](https://lit.dev/) library, and uses its _context_ functionality.
The component listens to various aspects of its environment (the page hosting it), and provides some standard contexts, for descendent web components to use.


## Contexts settled

* __User Interface context__ (ui-context), containing the following information:
    * _webpage_language_, extracted from _localStorage_ (key: _ui-context_lang_), or from _html/@lang_ attribute (in this order);

## Resources
[TypeScript template project without dependencies](https://github.com/aleph-naught2tog/ts_without_dependencies)
[Compilation](https://swc.rs/docs/configuration/compilation)
    