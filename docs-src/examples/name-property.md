---
layout: example.11ty.cjs
title: <kub-context> ⌲ Examples ⌲ Name Property
tags: example
name: Name Property
description: Setting the name property
---

<kub-context name="Earth"></kub-context>

<h3>HTML</h3>

```html
<kub-context name="Earth"></kub-context>
```
