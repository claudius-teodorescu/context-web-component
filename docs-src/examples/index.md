---
layout: example.11ty.cjs
title: <kub-context> ⌲ Examples ⌲ Basic
tags: example
name: Basic
description: A basic example
---

<style>
  kub-context p {
    border: solid 1px blue;
    padding: 8px;
  }
</style>
<kub-context>
  <p>This is child content</p>
</kub-context>

<h3>CSS</h3>

```css
p {
  border: solid 1px blue;
  padding: 8px;
}
```

<h3>HTML</h3>

```html
<kub-context>
  <p>This is child content</p>
</kub-context>
```
