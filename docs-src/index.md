---
layout: page.11ty.cjs
title: <kub-context> ⌲ Home
---

# &lt;kub-context>

`<kub-context>` is an awesome element. It's a great introduction to building web components with LitElement, with nice documentation site as well.

## As easy as HTML

<section class="columns">
  <div>

`<kub-context>` is just an HTML element. You can it anywhere you can use HTML!

```html
<kub-context></kub-context>
```

  </div>
  <div>

<kub-context></kub-context>

  </div>
</section>

## Configure with attributes

<section class="columns">
  <div>

`<kub-context>` can be configured with attributed in plain HTML.

```html
<kub-context name="HTML"></kub-context>
```

  </div>
  <div>

<kub-context name="HTML"></kub-context>

  </div>
</section>

## Declarative rendering

<section class="columns">
  <div>

`<kub-context>` can be used with declarative rendering libraries like Angular, React, Vue, and lit-html

```js
import {html, render} from 'lit-html';

const name = 'lit-html';

render(
  html`
    <h2>This is a &lt;kub-context&gt;</h2>
    <kub-context .name=${name}></kub-context>
  `,
  document.body
);
```

  </div>
  <div>

<h2>This is a &lt;kub-context&gt;</h2>
<kub-context name="lit-html"></kub-context>

  </div>
</section>
